package org.springframework.samples.petclinic.service.clientService;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.model.Client;
import org.springframework.samples.petclinic.service.ClientService;
import org.springframework.samples.petclinic.util.EntityUtils;

public abstract class AbstractClientServiceTests {

	@Autowired
	protected ClientService clientService;

	@Test
	public void shouldFindAllClients() {
		Collection<Client> clients = this.clientService.getAllClients();

		Client client1 = EntityUtils.getById(clients, Client.class, 1);
		assertThat(client1.getId()).isEqualTo(1);
		assertThat(client1.getNome()).isEqualTo("Son Goku");
	}
}
