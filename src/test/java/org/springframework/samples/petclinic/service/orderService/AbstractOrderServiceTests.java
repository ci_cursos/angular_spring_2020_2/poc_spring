package org.springframework.samples.petclinic.service.orderService;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.service.OrderService;
import org.springframework.samples.petclinic.util.EntityUtils;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractOrderServiceTests {

	@Autowired
	protected OrderService orderService;

	@Test
	public void shouldFindAllOrders() {
		Collection<Order> orders = this.orderService.findAll();

		Order order1 = EntityUtils.getById(orders, Order.class, 1);
		assertThat(order1.getId()).isEqualTo(1);
		assertThat(order1.getStatus()).isEqualTo("EM_ROTA");
		Order order2 = EntityUtils.getById(orders, Order.class, 2);
		assertThat(order2.getId()).isEqualTo(2);
		assertThat(order2.getStatus()).isEqualTo("EM_ROTA");
	}

	@Test
	public void shouldFindOrdersByDeliveryManId() {
		ArrayList<Order> orders=new ArrayList<>();
		orders.addAll(this.orderService.findAll(1).stream().collect(Collectors.toList()));

		assertThat(orders.get(0).getId()).isEqualTo(1);
		assertThat(orders.get(0).getStatus()).isEqualTo("EM_ROTA");
	}

    @Test
    @Transactional
    public void shouldFindOrderById() {
        Order order = new Order();
        order = this.orderService.findOrderById(1);

        assertThat(order.getId()).isEqualTo(1);
        assertThat(order.getStatus()).isEqualTo("EM_ROTA");
        assertThat(order.getTempoEstimado()).isEqualTo(35);
        assertThat(order.getLoja().getId()).isEqualTo(1);
        assertThat(order.getCliente().getNome()).isEqualTo("Son Goku");
        assertThat(order.getEntregador().getNome()).isEqualTo("Naruto Uzumaki");
        assertThat(order.getProducts().get(0).getNome()).isEqualTo("Creme Dental Oral-b Extra Fresh 70g Pack C/ 3 Unidades");
    }

    @Test
    @Transactional
    public void shouldInsertOrder() {
        Collection<Order> orders = this.orderService.findAll();
        int found = orders.size();

        List<Product> products = new ArrayList<Product>();

        Product p = new Product();
        p.setId(1);
        p.setNome("Creme Dental Oral-b Extra Fresh 70g Pack C/ 3 Unidades");
        p.setValor(7.0);
        p.setDesconto(0.0);
        p.setQuantidade(2);

        products.add(p);

        Store store = new Store();
        store.setId(1);
        store.setCep("90130-080");
        store.setLogradouro("Nunes Machado");
        store.setComplemento("");
        store.setBairro("Azenha");
        store.setLocalidade("Porto Alegre");
        store.setUf("RS");
        store.setLatitude(0.0);
        store.setLongitude(0.0);

        Client client = new Client();
        client.setId(1);
        client.setNome("Son Goku");
        client.setCep("90130-080");
        client.setLogradouro( "Nunes Machado");
        client.setComplemento("APTO 107");
        client.setBairro("Azenha");
        client.setLocalidade("Porto Alegre");
        client.setUf("RS");
        client.setLatitude(0.0);
        client.setLongitude(0.0);

        DeliveryMan dMan = new DeliveryMan();
        dMan.setId(1);
        dMan.setNome("Naruto Uzumaki");
        dMan.setStatus("EM_ROTA_DE_ENTREGA");
        dMan.setTelefone("90130-080");
        dMan.setLatitude(-50.23560);
        dMan.setLongitude(30.565780);

        Order o1 = new Order();
        o1.setId(5);
        o1.setStatus("RECEBIDO");
        o1.setTempoEstimado(35);
        o1.setLoja(store);
        o1.setCliente(client);
        o1.setEntregador(dMan);
        o1.setProducts(products);
        orders.add(o1);

        this.orderService.save(o1);
        assertThat(o1.getId().longValue()).isNotEqualTo(0);

        orders = this.orderService.findAll();
        assertThat(orders.size()).isEqualTo(found + 1);
    }

    @Test
    public void shouldFindAvailableOrdersByDeliveryManId() {
        ArrayList<Order> availableOrders =new ArrayList<>();
        availableOrders.addAll(this.orderService.findAvailableOrdersByDeliveryManId(1).stream().collect(Collectors.toList()));

        assertThat(availableOrders.get(0).getId()).isEqualTo(4);
        assertThat(availableOrders.get(0).getStatus()).isEqualTo("RECEBIDO");
    }

    @Test
    public void findAvailableOrders() {
        ArrayList<Order> availableOrders =new ArrayList<>();
        availableOrders.addAll(this.orderService.findAvailableOrders().stream().collect(Collectors.toList()));
        assertThat(availableOrders.get(0).getId()).isEqualTo(4);
        assertThat(availableOrders.get(0).getStatus()).isEqualTo("RECEBIDO");
    }

    @Test
    public void shouldGetDashboardData() {
        Dashboard dashboard = new Dashboard();
        dashboard = this.orderService.getDashboardData();

       if (dashboard.getTempoDeEntregaMedio() != 0.0f) {
            assertThat(dashboard.getTempoDeEntregaMedio()).isEqualTo(43.0f);
            assertThat(dashboard.getDistanciaMedia()).isEqualTo(10.0f);
       } else {
           assertThat(dashboard.getTempoDeEntregaMedio()).isEqualTo(0.0f);
           assertThat(dashboard.getDistanciaMedia()).isEqualTo(0.0f);
        }


    }
}
