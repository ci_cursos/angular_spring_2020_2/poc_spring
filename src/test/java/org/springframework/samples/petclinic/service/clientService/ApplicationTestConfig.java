package org.springframework.samples.petclinic.service.clientService;

import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestConfiguration;


public class ApplicationTestConfig {

	public ApplicationTestConfig(){
		MockitoAnnotations.initMocks(this);
	}

}
