package org.springframework.samples.petclinic.service.deliveryManService;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.model.DeliveryMan;
import org.springframework.samples.petclinic.service.DeliveryManService;

import java.util.Collection;


public abstract class AbstractDeliveryManServiceTests {

    @Autowired
    protected DeliveryManService deliveryManService;

    @Test
    public void shouldFindAllDeliveryMensTest() {
        Collection<DeliveryMan> deliveryMens = this.deliveryManService.findAll();
        Assertions.assertThat(deliveryMens.isEmpty()).isFalse();

    }

    @Test
    public void shouldFindDeliveryManById(){

        DeliveryMan deliveryMan = this.deliveryManService.findById(1);

        Assertions.assertThat(deliveryMan.getId()).isEqualTo(1);
    }


    @Test
    public void shouldInsertNewDeliveyMan(){
        DeliveryMan deliveryMan = this.deliveryManService.insert(new DeliveryMan("Relampago Marquinhos", "(51)981260254", "DISPONÍVEL", 0.0000001, 1.000001));
        Assertions.assertThat(deliveryMan.getNome()).isEqualTo("Relampago Marquinhos");
        Assertions.assertThat(deliveryMan.getTelefone()).isEqualTo("(51)981260254");
        Assertions.assertThat(deliveryMan.getStatus()).isEqualTo("DISPONÍVEL");

    }


}
