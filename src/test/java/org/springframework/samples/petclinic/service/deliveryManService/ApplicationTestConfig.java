package org.springframework.samples.petclinic.service.deliveryManService;

import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestConfiguration;


public class ApplicationTestConfig {
    public ApplicationTestConfig(){
        MockitoAnnotations.initMocks(this);
    }
}
