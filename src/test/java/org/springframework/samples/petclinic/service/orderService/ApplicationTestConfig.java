package org.springframework.samples.petclinic.service.orderService;

import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestConfiguration;


public class ApplicationTestConfig {

	public ApplicationTestConfig(){
		MockitoAnnotations.initMocks(this);
	}

}
