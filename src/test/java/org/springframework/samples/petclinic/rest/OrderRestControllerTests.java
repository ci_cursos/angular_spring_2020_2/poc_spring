package org.springframework.samples.petclinic.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.model.Client;
import org.springframework.samples.petclinic.model.Dashboard;
import org.springframework.samples.petclinic.model.DeliveryMan;
import org.springframework.samples.petclinic.model.Order;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.Product;
import org.springframework.samples.petclinic.model.Store;
import org.springframework.samples.petclinic.service.OrderService;
import org.springframework.samples.petclinic.service.clinicService.ApplicationTestConfig;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationTestConfig.class)
@WebAppConfiguration
public class OrderRestControllerTests {

    @InjectMocks
    private OrderRestController orderRestController;

    @MockBean
    private OrderService orderService;

    private MockMvc mockMvc;

    List<Order> orders = new ArrayList<Order>();

    Order order = new Order();

    Dashboard dashboard = new Dashboard();

    List<Order> availableOrders = new ArrayList<Order>();

	@Before
    public void initOrder(){
		MockitoAnnotations.initMocks(this);
        this.mockMvc =  MockMvcBuilders.standaloneSetup(orderRestController).setControllerAdvice(new ExceptionControllerAdvice())
            .build();

		List<Product> products = new ArrayList<Product>();

		Product p = new Product();
		p.setId(1);
		p.setNome("Creme Dental Oral-b Extra Fresh 70g Pack C/ 3 Unidades");
		p.setValor(7.0);
		p.setDesconto(0.0);
		p.setQuantidade(2);

		products.add(p);

		Store store = new Store();
		store.setId(1);
		store.setCep("90130-080");
		store.setLogradouro("Nunes Machado");
		store.setComplemento("");
		store.setBairro("Azenha");
		store.setLocalidade("Porto Alegre");
		store.setUf("RS");
		store.setLatitude(0.0);
		store.setLongitude(0.0);



		Client client = new Client();
		client.setId(1);
		client.setNome("Son Goku");
		client.setCep("90130-080");
		client.setLogradouro( "Nunes Machado");
		client.setComplemento("APTO 107");
		client.setBairro("Azenha");
		client.setLocalidade("Porto Alegre");
		client.setUf("RS");
		client.setLatitude(0.0);
		client.setLongitude(0.0);

		DeliveryMan dMan = new DeliveryMan();
		dMan.setId(1);
		dMan.setNome("Naruto Uzumaki");
		dMan.setStatus("EM_ROTA_DE_ENTREGA");
		dMan.setTelefone("90130-080");
		dMan.setLatitude(-50.23560);
		dMan.setLongitude(30.565780);

		Order o1 = new Order();
		o1.setId(1);
		o1.setStatus("RECEBIDO");
		o1.setTempoEstimado(35);
		o1.setLoja(store);
		o1.setCliente(client);
		o1.setEntregador(dMan);
		o1.setProducts(products);
		orders.add(o1);

		order = o1;

        Order o2 = new Order();
        o2.setId(2);
        o2.setStatus("RECEBIDO");
        o2.setTempoEstimado(37);
        o2.setLoja(store);
        o2.setCliente(client);
        o2.setEntregador(dMan);
        o2.setProducts(products);
        orders.add(o2);
        availableOrders.add(o2);

        Dashboard d2 = new Dashboard();
        d2.setDistanciaMedia(10);
        d2.setMaiorDistancia(10);
        d2.setMaiorTempoDeEntrega(43);
        d2.setMenorDistancia(10);
        d2.setMenorTempoDeEntrega(43);
        d2.setNumeroDePedidosHoje(4);
        d2.setNumeroTotalDePedidos(4);
        d2.setTempoDeEntregaMedio(43);

        dashboard = d2;



	}

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testGetAllOrdersByDeliveryMan() throws Exception {
        Mockito.when(orderService.findAll(1)).thenReturn(orders);
        this.mockMvc.perform(get("/api/pedidos/entregador/1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType("application/json"))
            .andExpect(jsonPath("$.[0].id").value(1))
            .andExpect(jsonPath("$.[0].loja.id").value(1))
            .andExpect(jsonPath("$.[0].cliente.id").value(1))
            .andExpect(jsonPath("$.[0].entregador.id").value(1))
            .andExpect(jsonPath("$.[0].products[0].id").value(1));
    }

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testGetAllAvailableOrdersByDeliveryManId() throws Exception {
        Mockito.when(orderService.findAvailableOrdersByDeliveryManId(1)).thenReturn(availableOrders);
        this.mockMvc.perform(get("/api/pedidos/disponiveis/entregador/1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType("application/json"))
            .andExpect(jsonPath("$.[0].id").value(2))
            .andExpect(jsonPath("$.[0].entregador.id").value(1));
    }

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testGetAllOrders() throws Exception{
    	 Mockito.when(orderService.findAll()).thenReturn(orders);
    	this.mockMvc.perform(get("/api/pedidos/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                .contentType("application/json"))
                .andExpect(jsonPath("$.[0].id").value(1))
                .andExpect(jsonPath("$.[0].loja.id").value(1))
                .andExpect(jsonPath("$.[0].cliente.id").value(1))
                .andExpect(jsonPath("$.[0].entregador.id").value(1))
                .andExpect(jsonPath("$.[0].products[0].id").value(1));
	}

    @Test
    @WithMockUser(roles="OWNER_ADMIN")
    public void testCreateOrderSuccess() throws Exception {
    	Order newOrder = orders.get(0);
    	newOrder.setId(1);
    	ObjectMapper mapper = new ObjectMapper();
    	String newOrderAsJSON = mapper.writeValueAsString(newOrder);
    	this.mockMvc.perform(post("/api/pedidos")
    		.content(newOrderAsJSON).accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
    		.andExpect(status().isCreated());

    }

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testfindAvailableOrders() throws Exception {
        Mockito.when(orderService.findAvailableOrders()).thenReturn(orders);
        this.mockMvc.perform(get("/api/pedidos/disponiveis")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType("application/json"))
            .andExpect(jsonPath("$.[0].id").value(1));
    }

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testFindOrderById() throws Exception {
        Mockito.when(orderService.findOrderById(1)).thenReturn(order);
        this.mockMvc.perform(get("/api/pedidos/1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType("application/json"))
            .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testUpdateOrderDman() throws Exception {
    	DeliveryMan dMan = new DeliveryMan();
		dMan.setId(2);
		dMan.setNome("Kakashi Hatake");
		dMan.setStatus("EM ENTREGA");
		dMan.setTelefone("90130-080");
		dMan.setLatitude(-50.23560);
		dMan.setLongitude(30.565780);

		given(this.orderService.findOrderById(1)).willReturn(orders.get(0));
    	Order order = orders.get(0);
    	order.setEntregador(dMan);
    	ObjectMapper mapper = new ObjectMapper();
    	String newOrderAsJSON = mapper.writeValueAsString(order);

    	this.mockMvc.perform(put("/api/pedidos/")
    		.content(newOrderAsJSON).accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        	.andExpect(content().contentType("application/json"))
        	.andExpect(status().isOk());

    	this.mockMvc.perform(get("/api/pedidos/1")
           	.accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))
            .andExpect(jsonPath("$.id").value(1))
            .andExpect(jsonPath("$.entregador.nome").value("Kakashi Hatake"));

    }

    @Test
    @WithMockUser(roles = "OWNER_ADMIN")
    public void testreturnDashboardData() throws Exception {
        Mockito.when(orderService.getDashboardData()).thenReturn(dashboard);
        this.mockMvc.perform(get("/api/pedidos/dashboard")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType("application/json"))
            .andExpect(jsonPath("$.menorDistancia").value(10))
            .andExpect(jsonPath("$.maiorDistancia").value(10))
            .andExpect(jsonPath("$.tempoDeEntregaMedio").value(43))
            .andExpect(jsonPath("$.menorTempoDeEntrega").value(43))
            .andExpect(jsonPath("$.maiorTempoDeEntrega").value(43))
            .andExpect(jsonPath("$.numeroTotalDePedidos").value(4))
            .andExpect(jsonPath("$.numeroDePedidosHoje").value(4))
            .andExpect(jsonPath("$.distanciaMedia").value(10));

    }



}
