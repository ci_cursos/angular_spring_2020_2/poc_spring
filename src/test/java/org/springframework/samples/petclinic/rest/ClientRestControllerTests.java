package org.springframework.samples.petclinic.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.model.Client;
import org.springframework.samples.petclinic.service.ClientService;
import org.springframework.samples.petclinic.service.clinicService.ApplicationTestConfig;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationTestConfig.class)
@WebAppConfiguration
public class ClientRestControllerTests {

	@InjectMocks
	private ClientRestController clientRestController;
	
	@MockBean
    private ClientService clientService;

	private MockMvc mockMvc;

	List<Client> clientes = new ArrayList<Client>();
	
	@Before
	public void initStores() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(clientRestController)
				.setControllerAdvice(new ExceptionControllerAdvice()).build();
			
		
		Client client = new Client();
		client.setId(1);
		client.setNome("Son Goku");
		client.setCep("90130-080");
		client.setLogradouro( "Nunes Machado");
		client.setComplemento("APTO 107");
		client.setBairro("Azenha");
		client.setLocalidade("Porto Alegre");
		client.setUf("RS");
		client.setLatitude(0.0);
		client.setLongitude(0.0);
		
		clientes.add(client);
	}

	@Test
	@WithMockUser(roles = "OWNER_ADMIN")
	public void testGetAllClientsSuccessAsOwnerAdmin() throws Exception {
		Mockito.when(clientService.getAllClients()).thenReturn(clientes);
		this.mockMvc.perform(get("/api/clientes").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.[0].id").value(1))
				.andExpect(jsonPath("$.[0].nome").value("Son Goku"))
				.andExpect(jsonPath("$.[0].cep").value("90130-080"))
				.andExpect(jsonPath("$.[0].logradouro").value("Nunes Machado"))
				.andExpect(jsonPath("$.[0].complemento").value("APTO 107"))
				.andExpect(jsonPath("$.[0].bairro").value("Azenha"))
				.andExpect(jsonPath("$.[0].localidade").value("Porto Alegre"))
				.andExpect(jsonPath("$.[0].uf").value("RS"))
				.andExpect(jsonPath("$.[0].latitude").value(0.0))
				.andExpect(jsonPath("$.[0].longitude").value(0.0));
	}

}
