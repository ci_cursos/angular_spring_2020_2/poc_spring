package org.springframework.samples.petclinic.rest;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.samples.petclinic.model.DeliveryMan;
import org.springframework.samples.petclinic.service.DeliveryManService;
import org.springframework.samples.petclinic.service.clinicService.ApplicationTestConfig;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationTestConfig.class)
@WebAppConfiguration
public class DeliveryManRestControllerTests {

	// Controlador usado
	@Autowired
	private DeliveryManRestController deliveryManController;

	@MockBean
	private DeliveryManService deliveryManService;

	private MockMvc mockMvc;

	private List<DeliveryMan> list;

	public DeliveryMan deliveryMan = new DeliveryMan();

	@Before
	public void initDeliveryMen() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(deliveryManController)
				.setControllerAdvice(new ExceptionControllerAdvice()).build();

		list = new ArrayList<DeliveryMan>();

		DeliveryMan dMan = new DeliveryMan();
		dMan.setId(1);
		dMan.setNome("Naruto da Silva");
		dMan.setTelefone("(51)9815-7898");
		dMan.setStatus("LIVRE");
		dMan.setLatitude(-50.23560);
		dMan.setLongitude(30.565780);
		list.add(dMan);
		deliveryMan = list.get(0);
	}

	@Test
	@WithMockUser(roles = "OWNER_ADMIN")
	public void testGetAllDeliveryMenSuccessAsOwnerAdmin() throws Exception {
		given(this.deliveryManService.findAll()).willReturn(list);
		this.mockMvc.perform(get("/api/entregadores/").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json")).andExpect(jsonPath("$.[0].id").value(1))
				.andExpect(jsonPath("$.[0].nome").value("Naruto da Silva"))
				.andExpect(jsonPath("$.[0].telefone").value("(51)9815-7898"))
				.andExpect(jsonPath("$.[0].status").value("LIVRE"));
	}

	@Test
	@WithMockUser(roles = "OWNER_ADMIN")
	public void testInsertDeliveryMan() throws Exception {
		DeliveryMan marquinhos = new DeliveryMan("Relampago Marquinhos", "(51)98126-0254", "livre", 0.000001, 1.00001);
		ObjectMapper mapper = new ObjectMapper();
		String newDeliverManAsJson = mapper.writeValueAsString(marquinhos);

		this.mockMvc
				.perform(post("/api/entregadores/novo").content(newDeliverManAsJson)
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());

	}

	@Test
	@WithMockUser(roles = "OWNER_ADMIN")
	public void testFindDeliveryManById() throws Exception {
		Mockito.when(deliveryManService.findById(1)).thenReturn(deliveryMan);
		this.mockMvc.perform(get("/api/entregadores/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json")).andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.nome").value("Naruto da Silva"));
	}

	@Test
	@WithMockUser(roles = "OWNER_ADMIN")
	public void testUpdatePosition() throws Exception {

		DeliveryMan dman = deliveryManService.findById(1);

		ObjectMapper mapper = new ObjectMapper();
		String newDeliverManAsJson = mapper.writeValueAsString(dman);
		Mockito.when(deliveryManService.findById(1)).thenReturn(deliveryMan);
		this.mockMvc
				.perform(put("/api/entregadores/atualizaposicaoentregador/1").param("latitude", "30.000")
						.param("longitude", "20.000").content(newDeliverManAsJson)
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.latitude").value(30.000)).andExpect(jsonPath("$.longitude").value(20.000));
	}
}
