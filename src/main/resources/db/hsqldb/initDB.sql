DROP TABLE vet_specialties IF EXISTS;
DROP TABLE vets IF EXISTS;
DROP TABLE specialties IF EXISTS;
DROP TABLE visits IF EXISTS;
DROP TABLE pets IF EXISTS;
DROP TABLE types IF EXISTS;
DROP TABLE owners IF EXISTS;
DROP TABLE roles IF EXISTS;
DROP TABLE users IF EXISTS;
DROP TABLE lojas IF EXISTS CASCADE;
DROP TABLE clientes IF EXISTS CASCADE;
DROP TABLE entregadores IF EXISTS CASCADE;
DROP TABLE produtos IF EXISTS CASCADE;
DROP TABLE pedidos IF EXISTS CASCADE;
DROP TABLE produto_pedido IF EXISTS CASCADE;

CREATE TABLE vets (
  id         INTEGER IDENTITY PRIMARY KEY,
  first_name VARCHAR(30),
  last_name  VARCHAR(30)
);
CREATE INDEX vets_last_name ON vets (last_name);

CREATE TABLE specialties (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(80)
);
CREATE INDEX specialties_name ON specialties (name);

CREATE TABLE vet_specialties (
  vet_id       INTEGER NOT NULL,
  specialty_id INTEGER NOT NULL
);
ALTER TABLE vet_specialties ADD CONSTRAINT fk_vet_specialties_vets FOREIGN KEY (vet_id) REFERENCES vets (id);
ALTER TABLE vet_specialties ADD CONSTRAINT fk_vet_specialties_specialties FOREIGN KEY (specialty_id) REFERENCES specialties (id);

CREATE TABLE types (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(80)
);
CREATE INDEX types_name ON types (name);

CREATE TABLE owners (
  id         INTEGER IDENTITY PRIMARY KEY,
  first_name VARCHAR(30),
  last_name  VARCHAR_IGNORECASE(30),
  address    VARCHAR(255),
  city       VARCHAR(80),
  telephone  VARCHAR(20)
);
CREATE INDEX owners_last_name ON owners (last_name);

CREATE TABLE pets (
  id         INTEGER IDENTITY PRIMARY KEY,
  name       VARCHAR(30),
  birth_date DATE,
  type_id    INTEGER NOT NULL,
  owner_id   INTEGER NOT NULL
);
ALTER TABLE pets ADD CONSTRAINT fk_pets_owners FOREIGN KEY (owner_id) REFERENCES owners (id);
ALTER TABLE pets ADD CONSTRAINT fk_pets_types FOREIGN KEY (type_id) REFERENCES types (id);
CREATE INDEX pets_name ON pets (name);

CREATE TABLE visits (
  id          INTEGER IDENTITY PRIMARY KEY,
  pet_id      INTEGER NOT NULL,
  visit_date  DATE,
  description VARCHAR(255)
);
ALTER TABLE visits ADD CONSTRAINT fk_visits_pets FOREIGN KEY (pet_id) REFERENCES pets (id);
CREATE INDEX visits_pet_id ON visits (pet_id);

CREATE  TABLE users (
  username    VARCHAR(20) NOT NULL ,
  password    VARCHAR(20) NOT NULL ,
  enabled     BOOLEAN DEFAULT TRUE NOT NULL ,
  PRIMARY KEY (username)
);

CREATE TABLE roles (
  id              INTEGER IDENTITY PRIMARY KEY,
  username        VARCHAR(20) NOT NULL,
  role            VARCHAR(20) NOT NULL
);
ALTER TABLE roles ADD CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username);
CREATE INDEX fk_username_idx ON roles (username);


CREATE TABLE lojas (
  id  INTEGER IDENTITY PRIMARY KEY,
  cep VARCHAR(30),
  logradouro  VARCHAR(30),
  complemento  VARCHAR(30),
  bairro  VARCHAR(30),
  localidade  VARCHAR(30),
  uf  VARCHAR(30),
  latitude  DOUBLE,
  longitude DOUBLE
);


CREATE INDEX lojas_id ON lojas (id);

CREATE TABLE clientes (
  id  INTEGER IDENTITY PRIMARY KEY,
  nome VARCHAR(30),
  cep VARCHAR(30),
  logradouro  VARCHAR(30),
  complemento  VARCHAR(30),
  bairro  VARCHAR(30),
  localidade  VARCHAR(30),
  uf  VARCHAR(30),
  latitude  DOUBLE,
  longitude  DOUBLE
);

CREATE INDEX clientes_id ON clientes (id);


CREATE TABLE entregadores (
  id  INTEGER IDENTITY PRIMARY KEY,
  nome VARCHAR(30),
  telefone VARCHAR(30),
  status  VARCHAR(30),
  latitude  DOUBLE,
  longitude  DOUBLE
);


CREATE INDEX entregadores_id ON entregadores (id);


CREATE TABLE produtos (
  id  INTEGER IDENTITY PRIMARY KEY,
  nome VARCHAR(250),
  valor NUMERIC,
  desconto  NUMERIC,
  quantidade  INTEGER
);


CREATE INDEX produtos_id ON produtos (id);


CREATE TABLE pedidos (
    id  INTEGER IDENTITY PRIMARY KEY,
    status VARCHAR(30),
    distancia INTEGER,
    tempo_inicial VARCHAR(30),
    tempo_retirada VARCHAR(30),
    tempo_finalizacao VARCHAR(30),
    tempo_entrega INTEGER,
    tempo_estimado VARCHAR(30),
    loja_id INTEGER NOT NULL,
    cliente_id INTEGER NOT NULL,
    entregador_id INTEGER
);

ALTER TABLE pedidos ADD CONSTRAINT fk_pedido_lojas_pedidos FOREIGN KEY (loja_id) REFERENCES lojas (id);
ALTER TABLE pedidos ADD CONSTRAINT fk_pedido_clientes_pedidos FOREIGN KEY (cliente_id) REFERENCES clientes (id);
ALTER TABLE pedidos ADD CONSTRAINT fk_pedido_entregadores_pedidos FOREIGN KEY (entregador_id) REFERENCES entregadores (id);

CREATE TABLE produto_pedido (
    produto_id  INTEGER NOT NULL,
    pedido_id INTEGER NOT NULL
);
