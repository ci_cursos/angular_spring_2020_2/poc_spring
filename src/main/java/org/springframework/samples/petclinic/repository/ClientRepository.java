package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.samples.petclinic.model.Client;

public interface ClientRepository {
    Collection<Client> findAll();
}
