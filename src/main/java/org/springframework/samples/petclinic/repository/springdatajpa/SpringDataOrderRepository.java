package org.springframework.samples.petclinic.repository.springdatajpa;


import java.util.Collection;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.Order;
import org.springframework.samples.petclinic.repository.OrderRepository;




public interface SpringDataOrderRepository extends OrderRepository, Repository<Order,Integer> {

    @Query(value = "SELECT o FROM Order o WHERE o.entregador.id = ?1")
    Collection<Order> findAll(Integer id) throws DataAccessException;

    @Query(value = "SELECT o FROM Order o WHERE o.entregador.id = ?1 AND o.status = 'RECEBIDO'")
    Collection<Order> findAvailableOrdersByDeliveryManId(Integer id) throws DataAccessException;
    
    @Query(value = "SELECT o FROM Order o WHERE o.status = 'RECEBIDO' ORDER BY o.tempoEstimado")
    Collection<Order> findAvailableOrders() throws DataAccessException;
    
    
    
}
