package org.springframework.samples.petclinic.repository;

import org.springframework.samples.petclinic.model.Order;
import java.util.Collection;

public interface OrderRepository {
    Collection<Order> findAll();
    void save(Order order);
    Collection<Order> findAll(Integer id);
    Collection<Order> findAvailableOrdersByDeliveryManId(Integer id);
    Order findById(Integer id);
    Collection<Order> findAvailableOrders();
}
