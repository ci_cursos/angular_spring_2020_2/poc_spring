package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.samples.petclinic.model.DeliveryMan;

public interface DeliveryManRepository {
	Collection<DeliveryMan> findAll();

	DeliveryMan findById(Integer id);

	DeliveryMan save(DeliveryMan deliveryMan);

}
