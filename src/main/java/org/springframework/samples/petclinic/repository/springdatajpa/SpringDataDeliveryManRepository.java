package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.DeliveryMan;
import org.springframework.samples.petclinic.repository.DeliveryManRepository;

public interface SpringDataDeliveryManRepository extends DeliveryManRepository, Repository<DeliveryMan, Integer> {



}
