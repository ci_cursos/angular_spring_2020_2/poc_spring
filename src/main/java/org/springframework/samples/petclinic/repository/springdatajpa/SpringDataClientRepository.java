package org.springframework.samples.petclinic.repository.springdatajpa;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.Client;
import org.springframework.samples.petclinic.repository.ClientRepository;

public interface SpringDataClientRepository extends ClientRepository, Repository<Client,Integer> {
    Collection<Client> findAll() throws DataAccessException;
}
