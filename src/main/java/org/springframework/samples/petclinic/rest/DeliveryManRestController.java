package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.DeliveryMan;
import org.springframework.samples.petclinic.model.Order;
import org.springframework.samples.petclinic.service.DeliveryManService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/entregadores")
public class DeliveryManRestController {

	@Autowired
	private DeliveryManService deliveryManService;

	@PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<DeliveryMan>> getAllDeliveryMen() {
		Collection<DeliveryMan> deliveryMan = new ArrayList<DeliveryMan>();

		deliveryMan.addAll(this.deliveryManService.findAll());

		return new ResponseEntity<Collection<DeliveryMan>>(deliveryMan, HttpStatus.OK);
	}

    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "/novo", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<DeliveryMan> InsertDeliveryMan(@RequestBody DeliveryMan deliveryMan){

	    ResponseEntity<DeliveryMan> response = new ResponseEntity<DeliveryMan>(this.deliveryManService.insert(deliveryMan), HttpStatus.CREATED);
        return response;
    }


    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "/atualizaposicaoentregador/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<DeliveryMan> updateDeliveryManPosition(@RequestParam String longitude,@RequestParam String latitude, @PathVariable Integer id){
    	
    	DeliveryMan deliveryMan = this.deliveryManService.findById(id);    	
    	
    	deliveryMan.setLatitude(Double.parseDouble(latitude));
    	deliveryMan.setLongitude(Double.parseDouble(longitude));

    	this.deliveryManService.update(deliveryMan);

	    ResponseEntity<DeliveryMan> response = new ResponseEntity<DeliveryMan>(deliveryMan, HttpStatus.OK);
        return response;
    }

	@PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<DeliveryMan> UpdateDeliveryMan(@RequestBody DeliveryMan deliveryMan){

		ResponseEntity<DeliveryMan> response = new ResponseEntity<DeliveryMan>(
				this.deliveryManService.update(deliveryMan), HttpStatus.OK);

		return response;
    }

    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<DeliveryMan> findDeliveryManById(@PathVariable Integer id) {
        DeliveryMan deliveryManById = new DeliveryMan();

        deliveryManById = this.deliveryManService.findById(id);

        return new ResponseEntity<DeliveryMan>(deliveryManById, HttpStatus.OK);
    }

}
