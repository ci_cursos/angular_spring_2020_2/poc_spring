package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Store;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/stores")
public class StoreRestController {
	
	
	@PreAuthorize( "hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)" )
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Store>> getAllStores(){
		Collection<Store> stores = new ArrayList<Store>();	
		
		Store s = new Store();
		
		s.setId(10345);
		s.setCep("90160-003");
		s.setLogradouro("Avenida Azenha, 1395");
		s.setComplemento("");
		s.setBairro("Azenha");
		s.setLocalidade("Porto Alegre");
		s.setUf("RS");
		s.setLatitude(51.21242415954055);
		s.setLongitude(-30.050322290313776);
		
		stores.add(s);
		
		return new ResponseEntity<Collection<Store>>(stores, HttpStatus.OK);
	}
}