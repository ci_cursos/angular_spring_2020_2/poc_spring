package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Client;
import org.springframework.samples.petclinic.service.ClientService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/clientes")
public class ClientRestController {
	
	@Autowired
	private ClientService clienteService;

	@PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Client>> getAllClients() {
		Collection<Client> clients = new ArrayList<Client>();
		clients = clienteService.getAllClients();
		if(clients == null){
			return new ResponseEntity<Collection<Client>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<Client>>(clients, HttpStatus.OK);
	}
}
