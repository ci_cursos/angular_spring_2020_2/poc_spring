package org.springframework.samples.petclinic.rest;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.Dashboard;
import org.springframework.samples.petclinic.model.Order;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.repository.OrderRepository;
import org.springframework.samples.petclinic.service.OrderService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import antlr.collections.List;

@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/pedidos")
public class OrderRestController {

    @Autowired
    private OrderService orderService;


    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Collection<Order>> getAllOrders() {
		Collection<Order> ordersClients = new ArrayList<>();
		ordersClients = orderService.findAll();
        if (ordersClients == null) {
            return new ResponseEntity<Collection<Order>>(HttpStatus.NOT_FOUND);
        }

		return new ResponseEntity<Collection<Order>>(ordersClients, HttpStatus.OK);
	}

    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "/entregador/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Collection<Order>> getAllOrdersByDeliveryMan(@PathVariable Integer id) {
        Collection<Order> ordersClients = new ArrayList<>();
        ordersClients = orderService.findAll(id);

        return new ResponseEntity<Collection<Order>>(ordersClients, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "/disponiveis/entregador/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Collection<Order>> findAvailableOrdersByDeliveryManId(@PathVariable Integer id) {
        Collection<Order> availableOrders = new ArrayList<>();
        availableOrders = orderService.findAvailableOrdersByDeliveryManId(id);

        return new ResponseEntity<Collection<Order>>(availableOrders, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Void> createOrder(@RequestBody Order order){
        orderService.save(order);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Order> findOrderById(@PathVariable Integer id) {
    	Order orderById = new Order();
        orderById = orderService.findOrderById(id);

        return new ResponseEntity<Order>(orderById, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order) {
    	orderService.save(order);
        return new ResponseEntity<Order>(order, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole(@roles.OWNER_ADMIN, @roles.VET_ADMIN)")
    @RequestMapping(value = "/disponiveis", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Collection<Order>> findAvailableOrders() {
        Collection<Order> availableOrders = new ArrayList<>();
        availableOrders = orderService.findAvailableOrders();
        return new ResponseEntity<Collection<Order>>(availableOrders, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Dashboard> returnDashboardData() {
        Dashboard dashboard = new Dashboard();
        dashboard = this.orderService.getDashboardData();

        return new ResponseEntity<Dashboard>(dashboard, HttpStatus.OK);
    }
}
