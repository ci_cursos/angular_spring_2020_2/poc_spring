package org.springframework.samples.petclinic.service;

import java.util.Collection;

import org.springframework.samples.petclinic.model.Client;

public interface ClientService {
	Collection<Client> getAllClients();
}
