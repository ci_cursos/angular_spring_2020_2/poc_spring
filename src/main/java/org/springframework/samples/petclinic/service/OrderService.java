package org.springframework.samples.petclinic.service;

import org.springframework.samples.petclinic.model.Dashboard;
import org.springframework.samples.petclinic.model.Order;
import java.util.Collection;

public interface OrderService {
	Collection<Order> findAll();
	void save(Order order);
	Collection<Order> findAll(Integer id);
    Collection<Order> findAvailableOrdersByDeliveryManId(Integer id);
    Order findOrderById(Integer id);
    Collection<Order> findAvailableOrders();
    Dashboard getDashboardData();
}
