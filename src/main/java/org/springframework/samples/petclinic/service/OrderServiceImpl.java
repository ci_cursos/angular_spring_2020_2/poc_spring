package org.springframework.samples.petclinic.service;

import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.model.Dashboard;
import org.springframework.samples.petclinic.model.Order;
import org.springframework.samples.petclinic.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService{

    private OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Collection<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public void save(Order order) {
       orderRepository.save(order);
    }

    @Override
    public Collection<Order> findAll(Integer id) {
        return orderRepository.findAll(id);
    }

    @Override
    public Collection<Order> findAvailableOrdersByDeliveryManId(Integer id) {
        return orderRepository.findAvailableOrdersByDeliveryManId(id);
    }

	@Override
	public Order findOrderById(Integer id) {
		return orderRepository.findById(id);
	}

	@Override
	public Collection<Order> findAvailableOrders() {
		return orderRepository.findAvailableOrders();
	}

    @Override
    public Dashboard getDashboardData() {
        Dashboard orderDashboard = new Dashboard();
        int somaDistancia = 0, somaTempo = 0, somaPedidosHoje = 0, count = 0;
        float mediaDistancia = 0.0f, mediaTempo = 0.0f;
        ArrayList<Integer> tempoEntrega = new ArrayList<>();
        ArrayList<Integer> distancia = new ArrayList<>();

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String strDateHoje = dateFormat.format(date);
        String dateSplit[] = strDateHoje.split(" ");
        strDateHoje = dateSplit[0];

        Collection<Order> orders = orderRepository.findAll();

        for (Order order : orders) {
            if(order.getStatus().equals("ENTREGUE")) {
                somaDistancia += order.getDistancia();
                somaTempo += order.getTempoDeEntrega();
                tempoEntrega.add(order.getTempoDeEntrega());
                distancia.add((order.getDistancia()));


                String strDateBd = order.getTempoInicial();
                String dateSplitBd[] = strDateBd.split(" ");
                strDateBd = dateSplitBd[0];

                count++;

                if (strDateHoje.equals(strDateBd)) {
                    somaPedidosHoje++;
                }
            }


        }
        if (distancia.size() != 0) {
            Collections.sort(distancia);
            int menorDistancia = distancia.get(0);
            int maiorDistancia = distancia.get(tempoEntrega.size()-1);
            orderDashboard.setMenorDistancia(menorDistancia);
            orderDashboard.setMaiorDistancia(maiorDistancia);

            mediaDistancia = somaDistancia / count;
            orderDashboard.setDistanciaMedia(mediaDistancia);

        }

        if (tempoEntrega.size() != 0) {
            Collections.sort(tempoEntrega);
            int menorTempoDeEntrega = tempoEntrega.get(0);
            int maiorTempoDeEntrega = tempoEntrega.get(tempoEntrega.size()-1);
            orderDashboard.setMenorTempoDeEntrega(menorTempoDeEntrega);
            orderDashboard.setMaiorTempoDeEntrega(maiorTempoDeEntrega);

            mediaTempo = somaTempo / count;
            orderDashboard.setTempoDeEntregaMedio(mediaTempo);
        }


        int totalPedidos = count;
        orderDashboard.setNumeroTotalDePedidos(totalPedidos);

        orderDashboard.setNumeroDePedidosHoje(somaPedidosHoje);

        if (count != 0) {
            return orderDashboard;
        } else {
            return new Dashboard();
        }
    }
}
