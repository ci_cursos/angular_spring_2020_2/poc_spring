package org.springframework.samples.petclinic.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.samples.petclinic.model.DeliveryMan;
import org.springframework.samples.petclinic.model.Order;
import org.springframework.samples.petclinic.repository.DeliveryManRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DeliveryManServiceImpl implements DeliveryManService {

    private DeliveryManRepository deliveryManRepository;

    @Autowired
    public DeliveryManServiceImpl(DeliveryManRepository deliveryManRepository) {
        this.deliveryManRepository = deliveryManRepository;
    }

    @Override
    public Collection<DeliveryMan> findAll() {
        return deliveryManRepository.findAll();
    }

    @Override
    public DeliveryMan findById(Integer id) {
        return deliveryManRepository.findById(id);
    }

    @Override
    public DeliveryMan insert(DeliveryMan deliveryMan) { 
    	return deliveryManRepository.save(deliveryMan); 
    }
    
  
	@Override
    public DeliveryMan update(DeliveryMan deliveryMan) { 
    	return deliveryManRepository.save(deliveryMan); 
    }
}
