package org.springframework.samples.petclinic.service;

import java.util.Collection;

import org.springframework.samples.petclinic.model.DeliveryMan;

public interface DeliveryManService {
	Collection<DeliveryMan> findAll();

	DeliveryMan findById(Integer id);

	DeliveryMan insert(DeliveryMan deliveryMan);

	DeliveryMan update(DeliveryMan deliveryMan);
}
