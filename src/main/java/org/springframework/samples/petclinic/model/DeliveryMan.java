package org.springframework.samples.petclinic.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "entregadores")
public class DeliveryMan extends BaseEntity implements Serializable {

	private String nome;
	private String telefone;
	private String status;
    private Double latitude;
    private Double longitude;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entregador", fetch = FetchType.EAGER)
    private Collection<Order> pedido;

    
    
    public DeliveryMan(String nome, String telefone, String status, Double latitude, Double longitude) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.status = status;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public DeliveryMan() {
    }

    public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
