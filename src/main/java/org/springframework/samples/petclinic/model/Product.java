package org.springframework.samples.petclinic.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "produtos")
public class Product extends BaseEntity implements Serializable {

    private String nome;
    private Double valor;
    private Double desconto;
    private Integer quantidade;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "produto_pedido",
        joinColumns = @JoinColumn(name = "produto_id"),
        inverseJoinColumns = @JoinColumn(name = "pedido_id")
    )
    List<Order> orders;

    public Product(String nome, Double valor, Double desconto, Integer quantidade) {
		super();
		this.nome = nome;
		this.valor = valor;
		this.desconto = desconto;
		this.quantidade = quantidade;
	}

	public Product() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
