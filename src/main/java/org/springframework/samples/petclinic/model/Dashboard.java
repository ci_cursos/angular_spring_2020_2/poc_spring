package org.springframework.samples.petclinic.model;

import java.io.Serializable;

public class Dashboard implements Serializable{

	float menorDistancia;
	float maiorDistancia;
	float tempoDeEntregaMedio;
	int menorTempoDeEntrega;
	int maiorTempoDeEntrega;
	int numeroTotalDePedidos;
	int numeroDePedidosHoje;
	float distanciaMedia;


	public Dashboard(float menorDistancia, float maiorDistancia, float tempoDeEntregaMedio, int menorTempoDeEntrega,
                     int maiorTempoDeEntrega, int numeroTotalDePedidos, int numeroDePedidosHoje, float distanciaMedia) {

		super();
		this.menorDistancia = menorDistancia;
		this.maiorDistancia = maiorDistancia;
		this.tempoDeEntregaMedio = tempoDeEntregaMedio;
		this.menorTempoDeEntrega = menorTempoDeEntrega;
		this.maiorTempoDeEntrega = maiorTempoDeEntrega;
		this.numeroTotalDePedidos = numeroTotalDePedidos;
		this.numeroDePedidosHoje = numeroDePedidosHoje;
		this.distanciaMedia = distanciaMedia;

	}

	public Dashboard() {
	}

	public float getDistanciaMedia() {
		return distanciaMedia;
	}
	public void setDistanciaMedia(float distanciaMedia) {
		this.distanciaMedia = distanciaMedia;
	}
	public float getMenorDistancia() {
		return menorDistancia;
	}
	public void setMenorDistancia(float menorDistancia) {
		this.menorDistancia = menorDistancia;
	}
	public float getMaiorDistancia() {
		return maiorDistancia;
	}
	public void setMaiorDistancia(float maiorDistancia) {
		this.maiorDistancia = maiorDistancia;
	}
	public float getTempoDeEntregaMedio() {
		return tempoDeEntregaMedio;
	}
	public void setTempoDeEntregaMedio(float tempoDeEntregaMedio) {
		this.tempoDeEntregaMedio = tempoDeEntregaMedio;
	}
	public int getMenorTempoDeEntrega() {
		return menorTempoDeEntrega;
	}
	public void setMenorTempoDeEntrega(int menorTempoDeEntrega) {
		this.menorTempoDeEntrega = menorTempoDeEntrega;
	}
	public int getMaiorTempoDeEntrega() {
		return maiorTempoDeEntrega;
	}
	public void setMaiorTempoDeEntrega(int maiorTempoDeEntrega) {
		this.maiorTempoDeEntrega = maiorTempoDeEntrega;
	}
	public int getNumeroTotalDePedidos() {
		return numeroTotalDePedidos;
	}
	public void setNumeroTotalDePedidos(int numeroTotalDePedidos) {
		this.numeroTotalDePedidos = numeroTotalDePedidos;
	}
	public int getNumeroDePedidosHoje() {
		return numeroDePedidosHoje;
	}
	public void setNumeroDePedidosHoje(int numeroDePedidosHoje) {
		this.numeroDePedidosHoje = numeroDePedidosHoje;
	}



}
