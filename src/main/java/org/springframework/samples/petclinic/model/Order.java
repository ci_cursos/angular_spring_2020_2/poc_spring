package org.springframework.samples.petclinic.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pedidos")
public class Order extends BaseEntity implements Serializable {

	private String status;

	private Integer distancia;

	@Column(name = "tempo_inicial")
	private String tempoInicial;

    @Column(name = "tempo_retirada")
    private String tempoDeRetirada;

    @Column(name = "tempo_finalizacao")
    private String tempoDeFinalizacao;

    @Column(name = "tempo_entrega")
    private Integer tempoDeEntrega;

	@Column(name = "tempo_estimado")
    private Integer tempoEstimado;

    @ManyToOne
    @JoinColumn(name = "loja_id")
    private Store loja;

    @ManyToOne
    @JoinColumn(name = "cliente_id")
    private Client cliente;

    @ManyToOne
    @JoinColumn(name = "entregador_id")
	private DeliveryMan entregador;

    @ManyToMany(mappedBy = "orders")
    private List<Product> products;


    public Order() {
    }

    public Integer getDistancia() {
        return distancia;
    }

    public void setDistancia(Integer distancia) {
        this.distancia = distancia;
    }

    public String getTempoInicial() {
        return tempoInicial;
    }

    public void setTempoInicial(String tempoInicial) {
        this.tempoInicial = tempoInicial;
    }

    public String getTempoDeRetirada() {
        return tempoDeRetirada;
    }

    public void setTempoDeRetirada(String tempoDeRetirada) {
        this.tempoDeRetirada = tempoDeRetirada;
    }

    public String getTempoDeFinalizacao() {
        return tempoDeFinalizacao;
    }

    public void setTempoDeFinalizacao(String tempoDeFinalizacao) {
        this.tempoDeFinalizacao = tempoDeFinalizacao;
    }

    public Integer getTempoDeEntrega() {
        return tempoDeEntrega;
    }

    public void setTempoDeEntrega(Integer tempoDeEntrega) {
        this.tempoDeEntrega = tempoDeEntrega;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTempoEstimado() {
        return tempoEstimado;
    }

    public void setTempoEstimado(Integer tempoEstimado) {
        this.tempoEstimado = tempoEstimado;
    }

    public Store getLoja() {
        return loja;
    }

    public void setLoja(Store loja) {
        this.loja = loja;
    }

    public Client getCliente() {
        return cliente;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public DeliveryMan getEntregador() {
        return entregador;
    }

    public void setEntregador(DeliveryMan entregador) {
        this.entregador = entregador;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
